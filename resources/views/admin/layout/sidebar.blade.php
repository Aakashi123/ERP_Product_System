<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <div class="search-panel">
            <div class="search-box">
              <div class="icon pull-left"><i class="fa fa-search"></i></div>
              <input class="text" type="text" placeholder="Special Search if require">
              <div class="clearfix"></div>
            </div>
        </div>
        <!-- Sidebar Menu-->
         <div class="sidebar-content">
            <ul class="sidebar-menu" id="nav-accordion">
                <ul class="sidebar-menu" id="nav-accordion">
                <li class=" @if(Request::segment(2) == 'dashboard') active @endif">
                    <a href="/admin/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    </a>
                </li>
                <li class=" @if(Request::segment(2) == 'users') active @endif">
                    <a href="<?=route('users.index',['type'=>'active'])?>">
                    <i class="fa fa-user"></i>
                    <span>Users</span>
                    </a>
                </li>
                <li class=" @if(Request::segment(2) == 'dynamic-form') active @endif">
                    <a href="/admin/dynamic-form">
                    <i class="fa fa-user"></i>
                    <span>Dynamic-form</span>
                    </a>
                </li>
                <li class=" @if(Request::segment(2) == 'import') active @endif">
                    <a href="/image/import">
                    <i class="fa fa-file"></i>
                    <span>Import</span>
                    </a>
                </li>
                <li class=" @if(Request::segment(2) == 'input') active @endif">
                    <a href="/admin/input">
                    <i class="fa fa-user"></i>
                    <span>Input type</span>
                    </a>
                </li>
                <li class="sub-menu @if(Request::segment(2) == 'master') active @endif">
                    <a href="javascript:;">
                        <i class="fa fa-bars"></i>
                        <span>Menu</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu @if(Request::segment(2) == 'master') menu-open @endif">
                        <li>
                            <a href="" class="@if(Request::segment(3) == 'events') active @endif">
                                <i class="fa fa-bars"></i>
                                <span>sub menu</span>
                                <i class="fa fa-angle-right"></i>
                            </a>
                            <ul class="treeview-menu @if(Request::segment(2) == 'master') menu-open @endif">
                                <li>
                                    <a href="" class="@if(Request::segment(3) == 'events') active @endif">
                                        <i class="fa fa-bars"></i>
                                        <span>tree menu</span>
                                    </a>
                                </li>
                            
                            </ul>
                        </li>
                        
                    </ul>
                </li>
                
                </ul>
                <li class="spacer"></li>
                <li class="logout">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span>Admin</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="logout-modal">
                        <li>
                            <a href="/admin/change-password">Change Password</a>
                        </li>
                        <li>
                            <a href="/admin/login">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </section>
</aside>