<header class="main-header hidden-print">
	<a class="logo" href="/admin/dashboard">
        <p class="large">
         	<img src="<?= asset('backend/images/admin.png') ?>">
        </p>
        <p class="small"></p>
    </a>
    <a class="sidebar-toggle" data-toggle="offcanvas"></a>
        @yield('top_fixed_content')
</header>