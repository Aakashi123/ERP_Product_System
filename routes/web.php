<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*----------Admin Route ---------- */ 

Route::get('admin/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login');

/*---------------------------------*/

Route::get('admin/password-change',function(){
	return view('admin.email');
});
Route::get('admin/password-reset',function(){
	return view('admin.reset');
});
Route::get('admin/change-password',function(){
	return view('admin.changepassword');
});
Route::get('admin/dashboard',function(){
	return view('admin.dashboard');
});
Route::resource('admin/users', 'UserController',['except' => ['show']]);
Route::get('admin/dynamic-form',function(){
	return view('admin.dynamic-form.new');
});

Route::get('image/import',function(){
	return view('admin.image.index');
});
Route::get('admin/input',function(){
	return view('admin.input-type.index');
});
Route::get('/admin/input/form',function(){
	return view('admin.input-type.new');
});
Route::get('admin/data',['as'=>'admin.data.index','uses'=>'UserController@data']);